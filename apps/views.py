from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request,'home.html')

def reportsinput(request):
    if request.method=='POST':

        inp=request.POST.get('inp')
        size=int(request.POST.get('size'))

        inp=inp.strip().split()
        st=""

        rows=1
        flag=0
        for i in inp:
            flag+=1
            st+="'"+i.strip()+"'"
            if flag != size:
                st+=','
            else:
                st+="\n"
                rows+=1
                flag=0
        
        return render(request,'reportsinput.html',{'data':st[:-1],'rows':rows})
    else:    
        return render(request,'reportsinput.html')


