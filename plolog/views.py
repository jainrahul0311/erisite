from django.shortcuts import render
from django.utils import timezone
from .models import Plo
# Create your views here.
def addplo(request):
    if request.method=='POST':
        inp=request.POST.get('inp')
        req=request.POST.get('req')
        action=request.POST.get('action')

        data=inp.strip().split()

        for i in data:
            p=Plo()
            p.plo=int(i)
            p.req=req
            p.action=action
            p.pub_date=timezone.datetime.now()
            p.save()
    
        return render(request,'addplo.html',{'msg':'Success','len':len(data)})
    else:
        return render(request,'addplo.html')