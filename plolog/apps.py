from django.apps import AppConfig


class PlologConfig(AppConfig):
    name = 'plolog'
