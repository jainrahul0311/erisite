from django.db import models

# Create your models here.
class Plo(models.Model):
    plo=models.IntegerField(default=0)
    req=models.CharField(max_length=50)
    action=models.CharField(max_length=50)
    pub_date=models.DateTimeField()

    def pub_date_pretty(self):
        return self.pub_date.strftime('%b %e %Y')